
# Morse Code

Morse code another challenge which is easy to decrypt with the morse translator or decoder

## Challenge Description

```
..-. .-.. .- --. ... .- -- ..- . .-.. -- --- .-. ... . .. ... -.-. --- --- .-.. -... -.-- - .... . .-- .- -.-- .. .-.. .. -.- . -.-. .... . . ...
```

## Solution

You can decode in the website from the [link](https://morsecode.world/international/translator.html)