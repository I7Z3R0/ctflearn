## Base 2 2 the 6

This is a basic base64 challenge which is easy to decrypt with cyberchef

## Challenge Description

```markdown
There are so many different ways of encoding and decoding information nowadays... One of them will work! Q1RGe0ZsYWdneVdhZ2d5UmFnZ3l9
```