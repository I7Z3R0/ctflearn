## Vigenere Cipher

This one more easy challenge to decode the same with the Vigenere Cipher with the key "blorpy"

## Challenge Description

```bash

The vignere cipher is a method of encrypting alphabetic text by using a series of interwoven Caesar ciphers based on the letters of a keyword.<br />

I’m not sure what this means, but it was left lying around: blorpy

gwox{RgqssihYspOntqpxs}
```

## Solution

Go the [link](https://cryptii.com/pipes/vigenere-cipher) and decode the provided flag with the KEY blorpy

