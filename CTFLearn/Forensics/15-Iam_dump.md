# I'm a dump

This is an easy challenge to find the flag with the strings

## Challenge Description

```
The keyword is hexadecimal, and removing an useless H.E.H.U.H.E. from the flag. The flag is in the format CTFlearn{*}
```

## Solution

```
➜  iam_dump strings file                                                                                                                         
/lib64/ld-linux-x86-64.so.2                                                                                                                      
libc.so.6                                                                                                                                        
__stack_chk_fail                                                                                                                                 
__cxa_finalize                                                                                                                                   
__libc_start_main                                                                                                                                
GLIBC_2.2.5                                                                                                                                      
GLIBC_2.4                                                                                                                                        
_ITM_deregisterTMCloneTable                                                                                                                      
__gmon_start__                                                                                                                                   
_ITM_registerTMCloneTable                                                                                                                        
u3UH                                                                                                                                             
CTFlearnH      																	// HERE IS FLAG                                                                                                                                  
{Redactedf                                                                                                                                        
l4g}H                                                                                                                                            
[]A\A]A^A_                                                                                                                                       
;*3$"        


```