# Minions

This is also basic level challenge but mainly about the extracting files and how to deal with the unrar files to extract important files from the images

## Challenge Description

```markdown
Hey! Minions have stolen my flag, encoded it few times in one cipher, and then hidden it somewhere there: https://mega.nz/file/1UBViYgD#kjKISs9pUB4E-1d79166FeX3TiY5VQcHJ_GrcMbaLhg Can you help me? TIP: Decode the flag until you got a sentence.
```

## Solution

We are provied with an image file from megadrive which we tried to check with the exiftool but unable to find anything interesting

Then used binwalk to extract the contents of the image which lead to one more megadrive link to download a different image file to investigate further.

```bash
➜  minions binwalk -e Hey_You.png

DECIMAL       HEXADECIMAL     DESCRIPTION
--------------------------------------------------------------------------------
0             0x0             PNG image, 1144 x 1056, 8-bit/color RGBA, non-interlaced
91            0x5B            Zlib compressed data, compressed
868059        0xD3EDB         RAR archive data, version 5.x

➜  minions ls -la
total 864
drwxr-xr-x  3 i7z3r0 i7z3r0   4096 Jul  1 16:11 .
drwxr-xr-x 22 i7z3r0 i7z3r0   4096 Jul  1 16:10 ..
-rw-r--r--  1 i7z3r0 i7z3r0 869428 Jul  1 16:10 Hey_You.png
drwxr-xr-x  2 i7z3r0 i7z3r0   4096 Jul  1 16:11 _Hey_You.png.extracted
➜  minions cd _Hey_You.png.extracted
➜  _Hey_You.png.extracted ls -la
total 868
drwxr-xr-x 2 i7z3r0 i7z3r0   4096 Jul  1 16:11 .
drwxr-xr-x 3 i7z3r0 i7z3r0   4096 Jul  1 16:11 ..
-rw-r--r-- 1 i7z3r0 i7z3r0     73 Jun 23  2020 ..txt
-rw-r--r-- 1 i7z3r0 i7z3r0      0 Jul  1 16:11 5B
-rw-r--r-- 1 i7z3r0 i7z3r0 869337 Jul  1 16:11 5B.zlib
-rw-r--r-- 1 i7z3r0 i7z3r0   1369 Jul  1 16:11 D3EDB.rar
➜  _Hey_You.png.extracted cat ..txt
https://mega.nz/file/wZw2nAhS#i3Q0r-R8psiB8zwUrqHTr661d8FiAS1Ott8badDnZko%

```

Downloaded the file to investigate further. Followed the same method which lead to another image file this time directly extracted. First checked with the exiftool which didnt give me anything

```
➜  minions mv ~/Downloads/Only_Few_Steps.jpg .
➜  minions file Only_Few_Steps.jpg
Only_Few_Steps.jpg: JPEG image data, JFIF standard 1.01, aspect ratio, density 1x1, segment length 16, Exif Standard: [TIFF image data, little-endian, direntries=1, software=Google], baseline, precision 8, 900x900, components 3
➜  minions binwalk -e Only_Few_Steps.jpg

DECIMAL       HEXADECIMAL     DESCRIPTION
--------------------------------------------------------------------------------
0             0x0             JPEG image data, JFIF standard 1.01
30            0x1E            TIFF image data, little-endian offset of first image directory: 8
426           0x1AA           Copyright string: "Copyright (c) 1998 Hewlett-Packard Company"
141318        0x22806         RAR archive data, version 5.x

➜  minions ls -la
total 1196
drwxr-xr-x  4 i7z3r0 i7z3r0   4096 Jul  1 16:14 .
drwxr-xr-x 22 i7z3r0 i7z3r0   4096 Jul  1 16:13 ..
-rw-r--r--  1 i7z3r0 i7z3r0 869428 Jul  1 16:10 Hey_You.png
-rw-r--r--  1 i7z3r0 i7z3r0 335359 Jul  1 16:13 Only_Few_Steps.jpg
drwxr-xr-x  3 i7z3r0 i7z3r0   4096 Jul  1 16:12 _Hey_You.png.extracted
drwxr-xr-x  2 i7z3r0 i7z3r0   4096 Jul  1 16:14 _Only_Few_Steps.jpg.extracted
➜  minions cd _Only_Few_Steps.jpg.extracted
➜  _Only_Few_Steps.jpg.extracted ls -la
total 400
drwxr-xr-x 2 i7z3r0 i7z3r0   4096 Jul  1 16:14  .
drwxr-xr-x 4 i7z3r0 i7z3r0   4096 Jul  1 16:14  ..
-rw-r--r-- 1 i7z3r0 i7z3r0 194041 Jul  1 16:14  22806.rar
-rw-r--r-- 1 i7z3r0 i7z3r0 204228 Jun 23  2020 'YouWon(Almost).jpg'
```

Extracted Youwon file again with binwalk which gave the below one. Since there is nothing with the exiftool then i tried to check the strings which gave me the password which was encoded with base64 multiple times

```
➜  _Only_Few_Steps.jpg.extracted binwalk -e YouWon\(Almost\).jpg

DECIMAL       HEXADECIMAL     DESCRIPTION
--------------------------------------------------------------------------------
0             0x0             JPEG image data, JFIF standard 1.01

➜  _Only_Few_Steps.jpg.extracted ls -la
total 400
drwxr-xr-x 2 i7z3r0 i7z3r0   4096 Jul  1 16:14  .
drwxr-xr-x 4 i7z3r0 i7z3r0   4096 Jul  1 16:14  ..
-rw-r--r-- 1 i7z3r0 i7z3r0 194041 Jul  1 16:14  22806.rar
-rw-r--r-- 1 i7z3r0 i7z3r0 204228 Jun 23  2020 'YouWon(Almost).jpg'
➜  _Only_Few_Steps.jpg.extracted ls -la
total 400
drwxr-xr-x 2 i7z3r0 i7z3r0   4096 Jul  1 16:14  .
drwxr-xr-x 4 i7z3r0 i7z3r0   4096 Jul  1 16:14  ..
-rw-r--r-- 1 i7z3r0 i7z3r0 194041 Jul  1 16:14  22806.rar
-rw-r--r-- 1 i7z3r0 i7z3r0 204228 Jun 23  2020 'YouWon(Almost).jpg'
➜  _Only_Few_Steps.jpg.extracted binwalk -e YouWon\(Almost\).jpg

➜  _Only_Few_Steps.jpg.extracted strings YouWon\(Almost\).jpg| grep -i ctf
CTF{VmtaU1IxUXhUbFZSYXpsV1RWUnNRMVpYZEZkYWJFWTJVVmhrVlZGVU1Eaz0=)
➜  _Only_Few_Steps.jpg.extracted 
➜  _Only_Few_Steps.jpg.extracted echo "VmtaU1IxUXhUbFZSYXpsV1RWUnNRMVpYZEZkYWJFWTJVVmhrVlZGVU1Eaz0=" | base64 -d | base64 -d | base64 -d | base64 -d
M1NI0NS_ARE_C00L%                                                                                                                      
➜  _Only_Few_Steps.jpg.extracted 

```