# Taking Ls

This is a basic challenge to understand about the hidden files.

## Challenge Description

```markdown
Just take the Ls. Check out this zip file and I be the flag will remain hidden. https://mega.nz/#!mCgBjZgB!_FtmAm8s_mpsHr7KWv8GYUzhbThNn0I8cHMBi4fJQp8
```

## Solution

Unzip the file and find the hidden folder with the password and then open the pdf to get the flag with the flag. Open the pdf with evince to get the flag


```
➜  talking_ls unzip The\ Flag.zip 
Archive:  The Flag.zip
   creating: The Flag/
  inflating: The Flag/.DS_Store      
   creating: __MACOSX/
   creating: __MACOSX/The Flag/
  inflating: __MACOSX/The Flag/._.DS_Store  
   creating: The Flag/.ThePassword/
  inflating: The Flag/.ThePassword/ThePassword.txt  
  inflating: The Flag/The Flag.pdf   
  inflating: __MACOSX/The Flag/._The Flag.pdf  
➜  talking_ls ls -la
total 36
drwxr-xr-x  4 i7z3r0 i7z3r0  4096 Jun 26 17:24  .
drwxr-xr-x 11 i7z3r0 i7z3r0  4096 Jun 26 01:58  ..
drwxr-xr-x  3 i7z3r0 i7z3r0  4096 Oct 30  2016 'The Flag'
-rw-r--r--  1 i7z3r0 i7z3r0 17441 Jun 26 17:19 'The Flag.zip'
drwxrwxr-x  3 i7z3r0 i7z3r0  4096 Oct 30  2016  __MACOSX
➜  talking_ls cat The\ Flag 
The\ Flag/     The\ Flag.zip
➜  talking_ls ls -la The\ Flag            
total 40
drwxr-xr-x 3 i7z3r0 i7z3r0  4096 Oct 30  2016  .
drwxr-xr-x 4 i7z3r0 i7z3r0  4096 Jun 26 17:24  ..
-rw-r--r-- 1 i7z3r0 i7z3r0  6148 Oct 30  2016  .DS_Store
drwxr-xr-x 2 i7z3r0 i7z3r0  4096 Oct 30  2016  .ThePassword
-rw-r--r-- 1 i7z3r0 i7z3r0 16647 Oct 30  2016 'The Flag.pdf'
➜  talking_ls cat The\ Flag/.ThePassword/ThePassword.txt 
Nice Job!  The Password is "Im The Flag".
```



