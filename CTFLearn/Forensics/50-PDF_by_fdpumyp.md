# PDF by fdpumyp

This is another simple challenge to find the flag from the pdf file. This is to extract the contents using the pdf-extract


## Challenge Description

```markdown
Hi, just as we talked during a break, you have this file here and check if something is wrong with it. That's the only thing we found strange with this suspect, I hope there will be a password for his external drive

Bye
```

## Solution 1:

Initially i solved this challenge with the xxd then later with the hint that i can also use pdf-extract -C option to read the contents.

```bash
➜  pdf_by_fdpumyp xxd dontopen.pdf 


00024a80: 5245 5420 4441 5441 2044 4f4e 5420 4c4f  RET DATA DONT LO                                                                    
00024a90: 4f4b 2041 5420 5448 4953 203d 3d0a 0a65  OK AT THIS ==..e                                                                    
00024aa0: 7874 6572 6e61 6c3a 5131 5247 6247 5668  xternal:Q1RGbGVh
00024ab0: 636d 3537 4b56 3878 6244 4233 4d33 6b77  cm57KV8xbDB3M3kw
00024ac0: 5657 3077 4d47 3135 4d54 497a 6651 3d3d  VW0wMG15MTIzfQ==										// This base64 gave me flag
00024ad0: 0a70 696e 3a31 3233 340a 7061 7373 776f  .pin:1234.passwo
00024ae0: 7264 3a4d 5449 7a4d 5664 5354 3035 484f  rd:MTIzMVdST05HO
00024af0: 576c 7a61 6d52 7555 4546 5455 3164 5055  WlzamRuUEFTU1dPU
00024b00: 6b51 3d0a 0a65 6e64 7374 7265 616d 0a65  kQ=..endstream.e
00024b10: 6e64 6f62 6a0a 0a78 7265 660a 3820 310a  ndobj..xref.8 1.
00024b20: 3030 3030 3134 3938 3737 2030 3030 3030  0000149877 00000
00024b30: 206e 200a 3133 2031 0a30 3030 3031 3530   n .13 1.0000150
00024b40: 3037 3920 3030 3030 3020 6e20 0a0a 7472  079 00000 n ..tr
00024b50: 6169 6c65 720a 3c3c 2f53 697a 6520 3134  ailer.<</Size 14
00024b60: 2f52 6f6f 7420 3820 3020 522f 496e 666f  /Root 8 0 R/Info
00024b70: 2031 2030 2052 2f50 7265 7620 3134 3935   1 0 R/Prev 1495
00024b80: 3339 3e3e 0a73 7461 7274 7872 6566 0a31  39>>.startxref.1
00024b90: 3530 3239 350a 2525 454f 460a            50295.%%EOF.
```


## Solution 2
```bash

pdf-parser -c dontopen.pdf
			...
			...
			...

  <<
    /Length 138
    /DL 138
    /Params
      <<
        /Size 138
      >>
  >>

 '== SECRET DATA DONT LOOK AT THIS ==\n\nexternal:Q1RGbGVhcm57KV8xbDB3M3kwVW0wMG15MTIzfQ==\npin:1234\npassword:MTIzMVdST05HOWlzamRuUEFTU1dPUkQ=\n\n'

xref

trailer
  <<
    /Size 14
    /Root 8 0 R
    /Info 1 0 R
    /Prev 149539
  >>

startxref 150295

PDF Comment '%%EOF\n'
```