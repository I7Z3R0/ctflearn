# PikesPeak

Again a simple forensic challenge to check on the strings

## Challenge Description


```markdown
Pay attention to those strings!
```


## Solution

```markdown
➜  pikespeak strings PikesPeak.jpg| head
JFIF
CTFLEARN{PikesPeak}
CTFLearn{Colorado}
%ctflearn{MountainMountainMountain}
#cTfLeArN{CTFMountainCTFmOUNTAIN}
CTF{AsPEN.Vail}
CTFlearn{Redacted}													// Flag is here and redacted
ctflearning{AUCKLAND}
ctfLEARN{MtDoom}
6ctflearninglearning{Mordor.TongariroAlpineCrossing}
```