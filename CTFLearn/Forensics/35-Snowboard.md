# Snowboard

Another easy challenge to make use of strings command


## Challenge Description

```
Find the flag in the jpeg file. Good Luck!
```


## Solution


```
➜  snowboard strings Snowboard.jpg| head
JFIF
CTFlearn{CTFIsEasy!!!}
ZmxhZ3tSZWRhY3RlZF9mbGFnfQo=								// Here is the flag if base64 decode, I have redacted the flag
Exif
Canon
Canon EOS 6D Mark II
GIMP 2.10.6
2019:05:07 14:37:21
0230
2018:08:23 12:52:08
➜  snowboard 
````