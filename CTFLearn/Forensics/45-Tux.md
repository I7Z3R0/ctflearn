# Tux

This is a basic challenge to make us use of a tool called binwalk, exiftool and unzip the important file

## Challenge Description

```markdown
The flag is hidden inside the Penguin! Solve this challenge before solving my 100 point Scope challenge which uses similar techniques as this one.
```

## Solution

We are provided with the image to investigate. Checked the image with the exiftool which had a strange comment secion which looked lika base64, Decrypting the base64 gave the result as `Password: Linux12345` which we made a note.

```bash
➜  tux exiftool Tux.jpg      
ExifTool Version Number         : 12.67
File Name                       : Tux.jpg
Directory                       : .
File Size                       : 5.7 kB
File Modification Date/Time     : 2024:06:29 07:58:06-04:00
File Access Date/Time           : 2024:06:29 07:59:01-04:00
File Inode Change Date/Time     : 2024:06:29 07:58:11-04:00
File Permissions                : -rw-r--r--
File Type                       : JPEG
File Type Extension             : jpg
MIME Type                       : image/jpeg
JFIF Version                    : 1.01
Resolution Unit                 : None
X Resolution                    : 1
Y Resolution                    : 1
Comment                         : ICAgICAgUGFzc3dvcmQ6IExpbnV4MTIzNDUK.							// Password: Linux12345
Image Width                     : 196
Image Height                    : 216
Encoding Process                : Baseline DCT, Huffman coding
Bits Per Sample                 : 8
Color Components                : 3
Y Cb Cr Sub Sampling            : YCbCr4:2:0 (2 2)
Image Size                      : 196x216
Megapixels                      : 0.042

```

Intiially tried to extract the image file with steghide with the password which had but unable to extract the same. Then used the binwalk to extract the image but unfortunately the flag file had nothing in it. Unzipped the file to check if there is something in it and found that the unzipping the file is asking for the password.

Entering the password for unzipping replaced the flag file with the correct flag in it.

```bash
➜  tux steghide extract -sf Tux.jpg
Enter passphrase:
steghide: could not extract any data with that passphrase!
➜  tux binwalk -e Tux.jpg

DECIMAL       HEXADECIMAL     DESCRIPTION
--------------------------------------------------------------------------------
0             0x0             JPEG image data, JFIF standard 1.01
5488          0x1570          Zip archive data, encrypted at least v1.0 to extract, compressed size: 39, uncompressed size: 27, name: flag
5679          0x162F          End of Zip archive, footer length: 22

➜  tux ls -la
total 20
drwxr-xr-x  3 i7z3r0 i7z3r0 4096 Jul  1 14:24 .
drwxr-xr-x 20 i7z3r0 i7z3r0 4096 Jun 29 07:58 ..
-rw-r--r--  1 i7z3r0 i7z3r0 5703 Jun 29 07:58 Tux.jpg
drwxr-xr-x  2 i7z3r0 i7z3r0 4096 Jul  1 14:24 _Tux.jpg.extracted
➜  tux ls -la _Tux.jpg.extracted
total 12
drwxr-xr-x 2 i7z3r0 i7z3r0 4096 Jul  1 14:24 .
drwxr-xr-x 3 i7z3r0 i7z3r0 4096 Jul  1 14:24 ..
-rw-r--r-- 1 i7z3r0 i7z3r0  215 Jul  1 14:24 1570.zip
-rw-r--r-- 1 i7z3r0 i7z3r0    0 Jul 21  2020 flag
➜  tux cd _Tux.jpg.extracted
➜  _Tux.jpg.extracted unzip 1570.zip
Archive:  1570.zip
[1570.zip] flag password:
replace flag? [y]es, [n]o, [A]ll, [N]one, [r]ename: y
 extracting: flag
➜  _Tux.jpg.extracted ls -la
total 16
drwxr-xr-x 2 i7z3r0 i7z3r0 4096 Jul  1 14:24 .
drwxr-xr-x 3 i7z3r0 i7z3r0 4096 Jul  1 14:24 ..
-rw-r--r-- 1 i7z3r0 i7z3r0  215 Jul  1 14:24 1570.zip
-rw-rw-r-- 1 i7z3r0 i7z3r0   27 Jul 21  2020 flag
➜  _Tux.jpg.extracted cat flag
CTFlearn{Redacted_flag}

```





