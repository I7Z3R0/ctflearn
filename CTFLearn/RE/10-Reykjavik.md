# Simple RE challenge which makes hands dirty in gdb

**Got the filename with Reykjavik.zip, unzipping got the 64 bit binary Reykjavik**

```bash
Reykjavik file Reykjavik
Reykjavik: ELF 64-bit LSB pie executable, x86-64, version 1 (SYSV), dynamically linked, interpreter /lib64/ld-linux-x86-64.so.2, BuildID[sha1]=9bc04368dbcefb4491573ac8feea3a32e31ed59f, for GNU/Linux 3.2.0, not stripped
```


**By running the program it expects an argument with the flag to check if the flag is correct or not.**

```bash
➜  Reykjavik ./Reykjavik 
Usage: Reykjavik CTFlearn{flag}
➜  Reykjavik ./Reykjavik CTFlearn{learn}
Welcome to the CTFlearn Reversing Challenge Reykjavik v2: CTFlearn{learn}
Compile Options: ${CMAKE_CXX_FLAGS} -O0 -fno-stack-protector -mno-sse

Sorry Dude, 'CTFlearn{learn}' is not the flag :-(

➜  Reykjavik 
```

**We need to take this to the gdb to crack this program and to bypass the check. If my assumption is right there is a string compare which is happening and if the string is right we will have strcmp =0 then it prints the key and if strcmp=1 then it says the key provided is incorrect.**

```bash

gdb -nx Reykjavik

(gdb) set disassembly-flavor intel

(gdb) disassemble main 

Dump of assembler code for function main:                                                                                                                    
   0x00000000000010a0 <+0>:     endbr64                                                                                                                      
   0x00000000000010a4 <+4>:     push   r13                                                                                                                   
   0x00000000000010a6 <+6>:     push   r12 
   (snip)
   0x0000000000001168 <+200>:   call   0x1080 <strcmp@plt>					## this is where the strcmp happens
   0x000000000000116d <+205>:   mov    r12d,eax
   0x0000000000001170 <+208>:   test   eax,eax						
   0x0000000000001172 <+210>:   jne    0x1197 <main+247>						## if the test succeed it gives access otherwise wrong flag, 
   (snip)
    0x0000000000001197 <+247>:   mov    rdx,rbp                                      ## Jumping here if the above fails
   0x000000000000119a <+250>:   mov    edi,0x1
   0x000000000000119f <+255>:   xor    eax,eax
   0x00000000000011a1 <+257>:   mov    r12d,0x4
   0x00000000000011a7 <+263>:   lea    rsi,[rip+0xf7a]        # 0x2128
   0x00000000000011ae <+270>:   call   0x1090 <__printf_chk@plt> 						## this line prints as incorrect flag

```


**Setting the break point to main and then we set a break to the function which test the jump conditions to understand the same**

```bash
(gdb) info breakpoints 
Num     Type           Disp Enb Address            What
1       breakpoint     keep y   0x00000000000010a0 <main>

(gdb) run CTFlearn{anything}

(gdb) info breakpoints 
Num     Type           Disp Enb Address            What
1       breakpoint     keep y   0x00005555555550a0 <main>
3       breakpoint     keep y   0x0000555555555170 <main+208>				// This is where the condition is being set
(gdb) 

```

**Now we see that the registers of rax is some random value now we need to set this register to 0 so that it meets the condition and doesnt jump further.**

```bash
(gdb) info registers 
rax            0xffffffe4          4294967268
rbx            0x7fffffffdd78      140737488346488
rcx            0x61                97

(gdb) set $eax=0
(gdb) info registers 
rax            0x0                 0												## this set rax to 0, eax is first 32 bit of rax so we are setting that to 0
rbx            0x7fffffffdd78      140737488346488
rcx            0x61                97

```


**After few attempts we got the flag **

```bash
(gdb) ni
0x0000555555555172 in main ()
(gdb) 
0x0000555555555174 in main ()
(gdb) 
0x0000555555555177 in main ()
(gdb) 
0x000055555555517e in main ()
(gdb) 
0x0000555555555183 in main ()
(gdb) 
0x0000555555555185 in main ()
(gdb) 
Congratulations, you found the flag!!: 'CTFlearn{Redacted_flag}

0x000055555555518a in main ()
(gdb) 
```

