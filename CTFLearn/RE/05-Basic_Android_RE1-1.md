# Basic Android RE1

This is a simple challenge to extract the RE and find the flag

## Challenge Description
```
A simple APK, reverse engineer the logic, recreate the flag, and submit!
```

## Solution

File MainActivity,smali contains the flag and middle flag with md5 hash encryption

```
➜  basic_android apktool d BasicAndroidRE1.apk -o BasicAndroidRE1                                                                                
Picked up _JAVA_OPTIONS: -Dawt.useSystemAAFontSettings=on -Dswing.aatext=true                                                                    
I: Using Apktool 2.7.0-dirty on BasicAndroidRE1.apk                                                                                              
I: Loading resource table...                                                                                                                     
I: Decoding AndroidManifest.xml with resources...                                                                                                
I: Loading resource table from file: /home/i7z3r0/.local/share/apktool/framework/1.apk                                                           
I: Regular manifest package...                                                                                                                   
I: Decoding file-resources...                                                                                                                    
I: Decoding values */* XMLs...                                                                                                                   
I: Baksmaling classes.dex...                                                                                                                     
I: Copying assets and libs...                                                                                                                    
I: Copying unknown files...                                                                                                                      
I: Copying original files...

cat BasicAndroidRE1/smali/com/example/secondapp/MainActivity.smali

```