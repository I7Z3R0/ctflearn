# Riyadh

Riyadh is a simple but a tricky challenge since the flag is hidden inside which is really difficult to get eventhough its represented as an easy challenge. It took me a while to understand everything. Even i am not sure if i did it in an intended way or not.


## Challenge description

```
Another entry level Reversing challenge, if you are new to Reversing you probably want to try my Reyjkavik challenge before attempting this challenge. Good Luck! The flag is hidden inside the Riyadh program. Solve the Challenge, get the flag, and I have included the encrypted sources used to create the challenge in the Riyadh.zip file. If you do to the work of solving the Challenge, I'm providing the Challenge source code (C++ and Python) if you are interested in studying the sources after solving the challenge. I think this is a great way to improve your Reversing skills when learning. Please don't share the sources or flag after you solve the challenge.
```

## Solution:

I took 5 break points before the je and jne to just continue the flow of the program, Then at the 0x00000000000011a8 cmp i just analyzed the values of rbp which gave me the flag.

```
➜  riyadh gdb -nx Riyadh
GNU gdb (Debian 13.2-1+b1) 13.2
Copyright (C) 2023 Free Software Foundation, Inc.
License GPLv3+: GNU GPL version 3 or later <http://gnu.org/licenses/gpl.html>
This is free software: you are free to change and redistribute it.
There is NO WARRANTY, to the extent permitted by law.
Type "show copying" and "show warranty" for details.
This GDB was configured as "x86_64-linux-gnu".
Type "show configuration" for configuration details.
For bug reporting instructions, please see:
<https://www.gnu.org/software/gdb/bugs/>.
Find the GDB manual and other documentation resources online at:
    <http://www.gnu.org/software/gdb/documentation/>.

For help, type "help".
Type "apropos word" to search for commands related to "word"...
Reading symbols from Riyadh...
(No debugging symbols found in Riyadh)
(gdb) set disassembly-flavor intel
(gdb) break *main
Breakpoint 1 at 0x1100
(gdb) break *0x000055555555513f
Breakpoint 2 at 0x0000000000001143
(gdb) break *0x0000555555555165
Breakpoint 3 at 0x555555555165
(gdb) break *0x0000555555555175
Breakpoint 4 at 0x555555555175
(gdb) break *0x00005555555551a8
Breakpoint 5 at 0x5555555551a8
(gdb) 0x00005555555551a8
Undefined command: "0x00005555555551a8".  Try "help".
(gdb) run CTFlearn{Reversing_Is_Easy}
Starting program: /home/i7z3r0/Desktop/CTF/ctflearn/riyadh/Riyadh CTFlearn{Reversing_Is_Easy}
[Thread debugging using libthread_db enabled]
Using host libthread_db library "/lib/x86_64-linux-gnu/libthread_db.so.1".

Breakpoint 1, 0x0000555555555100 in main ()
(gdb) continue
Continuing.
Welcome to CTFlearn Riyadh Reversing Challenge!
Compile Options: ${CMAKE_CXX_FLAGS} -O0 -fno-stack-protector -mno-sse

Breakpoint 2, 0x0000000000001143 in main ()
(gdb) set $r12d=1
(gdb) continue
Continuing.

Breakpoint 3, 0x0000555555555165 in main ()
(gdb) set $eax=1
(gdb) continue
Continuing.

Breakpoint 4, 0x0000555555555175 in main ()
(gdb) set $rax=30
(gdb) continue
Continuing.

Breakpoint 5, 0x00005555555551a8 in main ()
(gdb) x/s $rbp
0x55555556b6c0: "CTFlearn{Masmak_Fortress_1865}"
(
```